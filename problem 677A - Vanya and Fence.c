// GNU GCC C11 5.1.0
 
#include <stdio.h>
 
int main() {
    int n, h, a, w = 0;
    scanf("%d %d", &n, &h);
 
    for(int i = 0; i < n; i++) {
        scanf("%d", &a);
        w += (a > h)? 2 : 1;
    }
 
    printf("%d\n", w);
 
    return 0;
}