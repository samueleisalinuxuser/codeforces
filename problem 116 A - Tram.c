// GNU GCC C11 5.1.0
 
#include <stdio.h>
 
int main() {
    int n, a, b, m = 0, s = 0;
    scanf("%d", &n);
 
    for(int i = 0; i < n; i++) {
        scanf("%d %d", &a, &b);
        s += b - a;
        m = (s > m)? s : m;
    }
 
    printf("%d\n", m);
 
    return 0;
}