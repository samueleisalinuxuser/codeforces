import re
 
regex, subst = r"BG", "GB"
 
n, t = input().split(" ")
s = input();
 
result = re.sub(regex, subst, s, 0, re.MULTILINE)
for _ in range(int(t) - 1): result = re.sub(regex, subst, result, 0, re.MULTILINE)
 
print(result)