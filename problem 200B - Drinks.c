// GNU GCC C11 5.1.0
 
#include <stdio.h>
 
int main() {
    int n, p, sum = 0;
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        scanf("%d", &p);
        sum += p;
    }
    
    printf("%f\n", (float)sum/n);
 
    return 0;
}